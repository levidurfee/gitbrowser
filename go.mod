module github.com/golevi/gitbrowser

go 1.16

require (
	github.com/go-git/go-git/v5 v5.3.0
	github.com/gorilla/mux v1.8.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
