TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
TAG := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
VERSION := $(TAG:v%=%)
DATE := $(shell git log -1 --format=%cd --date=format:"%Y%m%d")
GO_VERSION := $(shell go version | sed "s/ /_/g")

FLAGS := -ldflags "-X main.version=$(TAG) -X main.commit=$(TAG_COMMIT) -X main.date=$(DATE) -X main.gov=$(GO_VERSION)"

.PHONY: clean build debian release package

clean:
	rm -Rf release

build:
	GOOS=linux GOARCH=amd64 go build $(FLAGS) -o release/$(VERSION)/gitbrowser/usr/bin/gitbrowser cmd/main.go

debian:
	mkdir -p release/$(VERSION)/gitbrowser/DEBIAN
	mkdir -p release/$(VERSION)/gitbrowser/usr/bin
	mkdir -p release/$(VERSION)/gitbrowser/etc/gitbrowser
	mkdir -p release/$(VERSION)/gitbrowser/usr/lib/systemd/system
	mkdir -p release/$(VERSION)/gitbrowser/usr/share/gitbrowser

	cp build/control release/$(VERSION)/gitbrowser/DEBIAN/control
	cp -R templates release/$(VERSION)/gitbrowser/usr/share/gitbrowser
	cp -R assets release/$(VERSION)/gitbrowser/usr/share/gitbrowser
	cp build/config.yml release/$(VERSION)/gitbrowser/etc/gitbrowser/config.yml
	cp build/gitbrowser.service release/$(VERSION)/gitbrowser/usr/lib/systemd/system/gitbrowser.service

	# Update version in Debian control file from the git tag
	sed -i 's/0.1.0/$(VERSION)/g' release/$(VERSION)/gitbrowser/DEBIAN/control

release: build debian package

package:
	dpkg-deb --build release/$(VERSION)/gitbrowser release
