package config

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// Config holds the configuration.
type Config struct {
	Path      string
	Port      string
	Templates string
	Assets    string
}

func GetConfig(file string) Config {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		panic(err)
	}

	var cfg Config

	data, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		panic(err)
	}

	if os.Getenv("GITBROWSER_PATH") != "" {
		cfg.Path = os.Getenv("GITBROWSER_PATH")
	}

	if os.Getenv("GITBROWSER_PORT") != "" {
		cfg.Port = os.Getenv("GITBROWSER_PORT")
	}

	if os.Getenv("GITBROWSER_TEMPLATES") != "" {
		cfg.Templates = os.Getenv("GITBROWSER_TEMPLATES")
	}

	if os.Getenv("GITBROWSER_ASSETS") != "" {
		cfg.Assets = os.Getenv("GITBROWSER_ASSETS")
	}

	if cfg.Path == "" {
		panic("Path required")
	}

	if cfg.Port == "" {
		panic("Port required")
	}

	if cfg.Templates == "" {
		panic("Templates required")
	}

	if cfg.Assets == "" {
		panic("Assets required")
	}

	return cfg
}
