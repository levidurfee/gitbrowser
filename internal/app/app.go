package app

import (
	"io/ioutil"
	"log"

	"github.com/go-git/go-git/v5"
	"github.com/golevi/gitbrowser/config"
	"github.com/golevi/gitbrowser/internal/models"
)

var Repositories map[string]models.Repository

func Start(cfg config.Config) {
	Repositories = make(map[string]models.Repository)

	items, err := ioutil.ReadDir(cfg.Path)
	if err != nil {
		panic(err)
	}

	for _, item := range items {
		// We only wanna deal with directories so if this isn't one, continue to
		// the next iteration.
		if !item.IsDir() {
			continue
		}

		repo, err := git.PlainOpen(cfg.Path + "/" + item.Name())
		if err != nil {
			log.Println(err)
		}

		cmiter, err := repo.Log(&git.LogOptions{})
		if err != nil {
			log.Println(err)
		}

		lc, err := cmiter.Next()
		if err != nil {
			log.Println(err)
		}
		commit := models.Commit{
			Hash: lc.Hash.String(),
			Author: models.User{
				Name:  lc.Author.Name,
				Email: lc.Author.Email,
			},
		}

		r := models.Repository{
			Name:         item.Name(),
			Repo:         repo,
			LatestCommit: commit,
		}

		Repositories[item.Name()] = r
	}
}
