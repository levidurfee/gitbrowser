package models

import "github.com/go-git/go-git/v5"

type Repos map[string]Repository

// Repository contains fields relating to a Git repository.
type Repository struct {
	Name string
	Repo *git.Repository
	Log  []Commit

	LatestCommit Commit
}
