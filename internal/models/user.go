package models

// User represents an author or committer.
type User struct {
	Name  string
	Email string
}
