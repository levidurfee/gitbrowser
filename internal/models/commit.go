package models

// Commit is a point-in-time record of changes to a Repository.
type Commit struct {
	Hash      string
	Message   string
	Author    User
	Committer User

	Files []File

	SimplePatch string

	ShortHash    string
	ShortMessage string

	AdditionPercentage string
}
