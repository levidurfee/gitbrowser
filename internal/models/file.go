package models

// File represents a file in a repository.
type File struct {
	Name string
}
