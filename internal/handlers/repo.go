package handlers

import (
	"html/template"
	"log"
	"net/http"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/golevi/gitbrowser/config"
	"github.com/golevi/gitbrowser/internal/app"
	"github.com/golevi/gitbrowser/internal/models"
	"github.com/gorilla/mux"
)

var (
	shortHashLength    = 8
	shortMessageLength = 50
)

func RepoHomeHandler(repos models.Repos, cfg config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tmpls, err := template.ParseFiles(cfg.Templates+"repo.tmpl", cfg.Templates+"base.tmpl")
		if err != nil {
			w.Write([]byte(err.Error()))
		}

		vars := mux.Vars(r)
		repo := app.Repositories[vars["repo"]]

		citer, err := repo.Repo.Log(&git.LogOptions{
			Order: git.LogOrderCommitterTime,
		})
		if err != nil {
			log.Println(err)
		}

		cl := []models.Commit{}
		citer.ForEach(func(c *object.Commit) error {
			msg := c.Message
			if len(c.Message) > shortMessageLength {
				msg = c.Message[:shortMessageLength]
			}

			cAuthor := c.Author
			author := models.User{
				Name:  cAuthor.Name,
				Email: cAuthor.Email,
			}

			cCommitter := c.Committer
			committer := models.User{
				Name:  cCommitter.Name,
				Email: cCommitter.Email,
			}

			commit := models.Commit{
				Hash:      c.Hash.String(),
				Message:   c.Message,
				Author:    author,
				Committer: committer,

				ShortHash:    c.Hash.String()[:shortHashLength],
				ShortMessage: msg,
			}
			cl = append(cl, commit)
			return nil
		})
		repo.Log = cl

		err = tmpls.Execute(w, repo)
		if err != nil {
			log.Println(err)
		}
	}
}
