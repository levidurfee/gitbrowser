package handlers

import (
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/golevi/gitbrowser/config"
	"github.com/golevi/gitbrowser/internal/app"
	"github.com/golevi/gitbrowser/internal/models"
	"github.com/gorilla/mux"
)

func CommitHandler(repos models.Repos, cfg config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tmpls, err := template.ParseFiles(cfg.Templates+"commit.tmpl", cfg.Templates+"base.tmpl")
		if err != nil {
			w.Write([]byte(err.Error()))
		}

		vars := mux.Vars(r)
		repo := app.Repositories[vars["repo"]]

		c, err := repo.Repo.CommitObject(plumbing.NewHash(vars["hash"]))
		if err != nil {
			log.Println(err)
		}
		msg := c.Message
		if len(c.Message) > shortMessageLength {
			msg = c.Message[:shortMessageLength]
		}

		cAuthor := c.Author
		author := models.User{
			Name:  cAuthor.Name,
			Email: cAuthor.Email,
		}

		cCommitter := c.Committer
		committer := models.User{
			Name:  cCommitter.Name,
			Email: cCommitter.Email,
		}

		fiter, err := c.Files()
		if err != nil {
			log.Println(err)
		}
		files := []models.File{}
		err = fiter.ForEach(func(f *object.File) error {
			file := models.File{
				Name: f.Name,
			}

			files = append(files, file)
			return nil
		})
		if err != nil {
			log.Println(err)
		}

		parent, err := c.Parent(0)
		if err != nil {
			log.Println(err)
		}
		patch := ""
		if parent != nil {
			ptch, err := parent.Patch(c)
			if err != nil {
				log.Println(err)
			}
			patch = ptch.String()
		}

		// @TODO
		// This block needs fixed. I'm not sure how to get the patch from the
		// beginning of the repo.
		//
		// 0000000000000000000000000000000000000000..604b28a66d440b25353532242ee23a720d27e4be
		//
		// Right now it's backwards, so it looks like the first commit only deletes
		// code instead of adding it.
		if parent == nil {
			ptch, err := c.Patch(nil)
			if err != nil {
				log.Println(err)
			}
			patch = ptch.String()
		}

		// Get stats, additions, deletions, etc
		stats, err := c.Stats()
		if err != nil {
			log.Println(err)
		}
		adds, dels := 0, 0
		for _, stat := range stats {
			adds += stat.Addition
			dels += stat.Deletion
		}
		total := adds + dels
		pcent := int((float32(adds) / float32(total) * 100))
		addPer := strconv.Itoa(pcent)

		cmmit := models.Commit{
			Hash:               c.Hash.String(),
			Message:            msg,
			Author:             author,
			Committer:          committer,
			Files:              files,
			SimplePatch:        patch,
			AdditionPercentage: addPer,
		}

		err = tmpls.Execute(w, struct {
			Name   string
			Commit models.Commit
		}{
			Name:   repo.Name,
			Commit: cmmit,
		})
		if err != nil {
			log.Println(err)
		}
	}

}
