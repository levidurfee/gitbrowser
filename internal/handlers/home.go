package handlers

import (
	"html/template"
	"log"
	"net/http"

	"github.com/golevi/gitbrowser/config"
	"github.com/golevi/gitbrowser/internal/models"
)

// HomeHandler returns the handler for the homepage
func HomeHandler(repos models.Repos, cfg config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tmpls, err := template.ParseFiles(cfg.Templates+"home.tmpl", cfg.Templates+"base.tmpl")
		if err != nil {
			w.Write([]byte(err.Error()))
		}

		err = tmpls.Execute(w, repos)
		if err != nil {
			log.Println(err)
		}
	}
}
