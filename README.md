# GitBrowser

## Support/Questions

Join the [mailing list](https://lists.xsixc.net/mailman/listinfo/gogit_lists.xsixc.net).

## Config

There are two ways to configure GitBrowser. You can configure it using a YAML
file and/or environment variables. Any environment variables you set WILL change
the value set from the YAML file.

### File

```yaml
port: "8080"
path: "/srv/git"
templates: "/usr/share/gitbrowser/templates/"
assets: "/usr/share/gitbrowser/assets/"
```

### Environment Variables

```sh
export GITBROWSER_PORT=8080
export GITBROWSER_PATH=/srv/git
export GITBROWSER_TEMPLATES=/usr/share/gitbrowser/templates/
export GITBROWSER_ASSETS=/usr/share/gitbrowser/assets/
```
