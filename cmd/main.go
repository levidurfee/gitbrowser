package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/golevi/gitbrowser/config"
	"github.com/golevi/gitbrowser/internal/app"
	"github.com/golevi/gitbrowser/internal/handlers"
	"github.com/gorilla/mux"
)

var (
	version = ""
	commit  = ""
	date    = ""
	gov     = ""
)

func main() {
	fmt.Printf("\nGitBrowser\n")
	fmt.Printf("======================================\n")
	fmt.Printf("Version: %s\n", version)
	fmt.Printf("Commit:  %s\n", commit)
	fmt.Printf("Built:   %s\n", date)
	fmt.Printf("Go:      %s\n", gov)
	fmt.Printf("======================================\n\n")

	configOption := flag.String("config", "/etc/gitbrowser/config.yml", "path to config file")
	flag.Parse()

	log.Println(*configOption)

	cfg := config.GetConfig(*configOption)
	app.Start(cfg)

	r := mux.NewRouter()

	r.HandleFunc("/", handlers.HomeHandler(app.Repositories, cfg))
	r.HandleFunc("/r/{repo}", handlers.RepoHomeHandler(app.Repositories, cfg))
	r.HandleFunc("/r/{repo}/{hash}", handlers.CommitHandler(app.Repositories, cfg))

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(cfg.Assets))))

	srv := &http.Server{
		Handler:      r,
		Addr:         ":" + cfg.Port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Printf("Listening on port %s\n", cfg.Port)

	log.Fatal(srv.ListenAndServe())
}
